//
//  Concentration.swift
//  ProgrammacticUI
//
//  Created by Gourob Mazumder on 9/9/22.
//

import Foundation

class Concentration{
    
    var cards = [Card]()
    var score = 0
    var indexOfOneAndOnlyFaceUpCard : Int?
    var remainingCard : Int = 0
    
    func chooseCard(at index: Int){
        if !cards[index].isMatched{
            if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index {
                
                //check if cards match
                if cards[matchIndex].identifier == cards[index].identifier{
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                    
                    score += 2
                    remainingCard -= 2
                    print(remainingCard)
                }else{
                    score -= 1
                }
                
                cards[index].isfaceUp = true
                indexOfOneAndOnlyFaceUpCard = nil
                
            }else{
                //either no cards or 2 cards are face up
                for flipDownIndex in cards.indices{
                    cards[flipDownIndex].isfaceUp = false
                }
                cards[index].isfaceUp = true
                indexOfOneAndOnlyFaceUpCard = index
            }
        }
        
    }
    
    init(numberOfPairsOfCards: Int) {
        for _ in 0..<numberOfPairsOfCards
        {
            let card = Card()
            cards += [card, card]
        }
        remainingCard = cards.count
        cards.shuffle()
    }
}
