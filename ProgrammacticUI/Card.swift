//
//  Card.swift
//  ProgrammacticUI
//
//  Created by Gourob Mazumder on 9/9/22.
//

import Foundation


struct Card
{
    var isfaceUp = false
    var isMatched = false
    var identifier : Int
    
    static var identifierFactory = 0
    
    static func getUniqeIdentifier() -> Int{
        identifierFactory += 1
        return identifierFactory
    }
    
    init() {
        self.identifier = Card.getUniqeIdentifier()
    }
}
