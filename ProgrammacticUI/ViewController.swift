//
//  ViewController.swift
//  ProgrammacticUI
//
//  Created by Gourob Mazumder on 8/9/22.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var game = Concentration(numberOfPairsOfCards : (allCards.count + 1) / 2)
    
    lazy var allCards =  [UIButton]()
    lazy var restartButton = UIButton()
    lazy var scoreLabel = UILabel()
    lazy var flipsLabel = UILabel()
    
    var animalEmoji = ["🐶","🦊","🦋","🦧"]
    var sportsEmoji = ["⚽️","🏀","🎱","🏑"]
    var facesEmoji  = ["😃","😭","😡","🥶"]
    var halloweenEmoji = ["👻","🎃","😈","👽"]
    var themes = [[String]]()
    lazy var emojiChoices = [ String ]()
    
    
    var flipCount = 0 {
        didSet{
            flipsLabel.text = String("Flips: \(flipCount)")
        }
    }
    
    
    @IBAction func restartButtonTapped(){
        restartButton.isHidden = true
        flipCount = 0
        game = Concentration(numberOfPairsOfCards: (allCards.count+1)/2)
        
        scoreLabel.text = "Score : " + String(game.score)
        emojiChoices = themes[ Int.random(in: 0..<themes.count) ]
        
        for index in allCards.indices{
            allCards[index].isHidden = false
        }
        updateViewFromModel()
    }
    
    
    
    @IBAction func buttonTapped(_ sender: UIButton){
        flipCount += 1
        if let index = allCards.firstIndex(of: sender){
            game.chooseCard(at: index)
        }
        
        if game.remainingCard == 0 {
            restartButton.isHidden = false
            scoreLabel.text = "Score : " + String(game.score)
            
            for index in allCards.indices{
                allCards[index].isHidden = true
            }
        }
        updateViewFromModel()
    }
    
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let buttonArea = view.frame.size.height * 0.7
        let statusArea = view.frame.size.height - buttonArea
        
        
        
        let btw = view.frame.size.width / 3.4
        let gapWidth = (view.frame.size.width - (btw*3)) / 4.0
        
        let bth = buttonArea/3.4
        let gapHeight = ( buttonArea - (3 * bth) ) / 4
        
        //first row
        for i in 0..<3{
           
            let tempBtn = UIButton(frame: CGRect(x: (CGFloat(i+1) * gapWidth) + (CGFloat(i) * btw), y: gapHeight, width: btw, height: bth))
            tempBtn.backgroundColor = .orange
            tempBtn.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
            tempBtn.titleLabel?.font = .systemFont(ofSize: 30)
            allCards += [tempBtn]
            view.addSubview(tempBtn)
        }
        
        
        //second row
        for i in 0..<3{
            
            let tempBtn = UIButton(frame: CGRect(x: (CGFloat(i+1) * gapWidth) + (CGFloat(i) * btw), y: 2 * gapHeight + bth , width: btw, height: bth))
            tempBtn.backgroundColor = .orange
            tempBtn.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
            tempBtn.titleLabel?.font = .systemFont(ofSize: 30)
            allCards += [tempBtn]
            view.addSubview(tempBtn)
        }
        
        
        //third row
        for i in 0..<2{
            let tempBtn = UIButton(frame: CGRect(x: (CGFloat(i+1) * gapWidth) + (CGFloat(i) * btw), y: (3 * gapHeight) + (2 * bth) , width: btw, height: bth))
            tempBtn.backgroundColor = .orange
            tempBtn.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
            tempBtn.titleLabel?.font = .systemFont(ofSize: 30)
            allCards += [tempBtn]
            view.addSubview(tempBtn)
        }
        
        
        
        themes = [animalEmoji,sportsEmoji, facesEmoji, halloweenEmoji]
        emojiChoices = themes[ Int.random(in: 0..<themes.count) ]
        
        
        
        restartButton = UIButton(frame: CGRect(x: view.frame.size.width / 2 - 50, y: view.frame.size.height / 2, width: 100  , height: 50))
        restartButton.setTitle("Restart", for: .normal)
        restartButton.backgroundColor = .systemBlue
        restartButton.isHidden = true
        restartButton.addTarget(self, action: #selector(restartButtonTapped), for: .touchUpInside)
        
        view.addSubview(restartButton)
        
        

        
        let statusLength = statusArea / 2.4
        let statusGap = statusArea - ( statusLength * 2 )
        
        
        //scorelabel + flipslabel
        scoreLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 10, y: buttonArea + statusGap , width: view.frame.size.width, height: statusLength))
        scoreLabel.font = UIFont.boldSystemFont(ofSize: statusLength / 2 )
        scoreLabel.center.x = view.center.x
        scoreLabel.textAlignment = .center
        flipsLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 10, y: buttonArea + statusLength + statusGap , width: view.frame.size.width, height: statusLength))
        flipsLabel.font = UIFont.boldSystemFont(ofSize: statusLength / 2)
        flipsLabel.center.x  = view.center.x
        flipsLabel.textAlignment = .center
        view.addSubview(scoreLabel)
        view.addSubview(flipsLabel)
    }
    
    
    
    func updateViewFromModel()  {
        scoreLabel.text = "Score : " + String(game.score)
        for index in allCards.indices{
            let button = allCards[index]
            let card = game.cards[index]
            
            if card.isfaceUp{
                button.setTitle(emoji(for : card), for: .normal)
                button.backgroundColor = .white
            }else{
                button.setTitle("",for: .normal)
                button.backgroundColor = card.isMatched ? .clear : .orange
            }
        }
    }
    
    
    
    
    
    
    var emoji = Dictionary<Int, String>()
    
    
    func emoji(for card: Card)-> String {
        if emoji[card.identifier] == nil, emojiChoices.count > 0 {
            let randomIndex = Int(arc4random_uniform(UInt32(emojiChoices.count)))
            emoji[card.identifier] = emojiChoices.remove(at: randomIndex)
        }
        return emoji[card.identifier] ?? "?"
    }
}

